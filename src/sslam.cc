//----------------------------------------
//imports
//----------------------------------------
#include "sslam.h"


//----------------------------------------
//class defs
//----------------------------------------
namespace sslam {
  
  class Utilities {

    public:
      static std::vector<std::string> get_paths_from_dir (std::string directory_s) {
        std::vector<std::string> vPaths_s;
        for (boost::filesystem::directory_entry& entry : boost::filesystem::directory_iterator(directory_s)) {
          if (entry.path().string().find(".jpg") || entry.path().string().find(".png") != std::string::npos) {
            vPaths_s.push_back(entry.path().string());
          }
        }

        std:sort(vPaths_s.begin(), vPaths_s.end());
        return vPaths_s;
      }

      static void preview_stereo_data (std::vector<std::string> vPathsLeft_s, std::vector<std::string> vPathsRight_s, int fps_i = 24) {
        int size = vPathsLeft_s.size();
        auto iterLeft = vPathsLeft_s.begin();
        auto iterRight = vPathsRight_s.begin();

        while (size--) {
          std::string pathLeft_s = *iterLeft;
          std::string pathRight_s = *iterRight;
          cv::Mat imageLeft_o = cv::imread(pathLeft_s);
          cv::Mat imageRight_o = cv::imread(pathRight_s);
          cv::imshow("left", imageLeft_o);
          cv::imshow("right", imageRight_o);
          cv::waitKey(int(1000/fps_i));

          ++iterLeft;
          ++iterRight;
        }
      }

      static void parse_config (std::string path) {
        //do later        
      }

    private:

  };

  class MapPoint {

    public:

      //vars
      Eigen::Vector3d position_o;
      Eigen::Vector3d normal_o;
      cv::Mat descriptor_o;

      MapPoint (Eigen::Vector3d position_o, cv::Mat descriptor_o) : position_o(position_o), descriptor_o(descriptor_o) {

      }

      void update_position (Eigen::Vector3d position_o) {
        this->position_o = position_o;
      }

      void update_normal (Eigen::Vector3d normal_o) {
        this->normal_o = normal_o;
      }

      void update_descriptor (cv::Mat descriptor_o) {
        this->descriptor_o = descriptor_o;
      }

    private:

  };

  class Optimizer {

    public:

      //vars
      g2o::SparseOptimizer optimizer_o;
      double gainTerminateThreshold;
      double robustCostFunctionDelta;
      double focal1, focal2, point1, point2, baseline;
      int vertIdxGenerator_i;

      Optimizer (double focal1, double focal2, double point1, double point2, double baseline) : focal1(focal1), focal2(focal2), point1(point1), point2(point2), baseline(baseline) {
        std::unique_ptr<g2o::BlockSolver_6_3::LinearSolverType> linearSolver(new g2o::LinearSolverCSparse<g2o::BlockSolver_6_3::PoseMatrixType>());
        std::unique_ptr<g2o::BlockSolver_6_3> solver_ptr(new g2o::BlockSolver_6_3(std::move(linearSolver)));
        g2o::OptimizationAlgorithmLevenberg* solver = new g2o::OptimizationAlgorithmLevenberg(std::move(solver_ptr));
        optimizer_o.setAlgorithm(solver);

        gainTerminateThreshold = 1e-6;
        g2o::SparseOptimizerTerminateAction* terminateAction = new g2o::SparseOptimizerTerminateAction();
        terminateAction->setGainThreshold(gainTerminateThreshold);
        optimizer_o.addPostIterationAction(terminateAction);

        robustCostFunctionDelta = std::sqrt(5.991);

        vertIdxGenerator_i = 0;

      }

      g2o::VertexCam* add_pose (Eigen::Isometry3d &pose_o, bool isFixed_b) {
        Eigen::Vector3d trans(pose_o.translation());
        Eigen::Quaterniond rot(pose_o.rotation());
        g2o::SBACam sbacam(rot, trans);
        sbacam.setKcam(focal1, focal2, point1, point2, baseline);

        g2o::VertexCam* v_se3 = new g2o::VertexCam();
        v_se3->setId(vertIdxGenerator_i++);
        v_se3->setEstimate(sbacam);
        v_se3->setFixed(isFixed_b);

        optimizer_o.addVertex(v_se3);
        return v_se3;
      }

      g2o::VertexPointXYZ* add_point (MapPoint &mappoint, bool isFixed_b, bool isMarginalized_b=true) {
        g2o::VertexPointXYZ* v_p = new g2o::VertexPointXYZ();
        v_p->setId(vertIdxGenerator_i++);
        v_p->setMarginalized(isMarginalized_b);
        v_p->setEstimate(mappoint.position_o);
        v_p->setFixed(isFixed_b);

        optimizer_o.addVertex(v_p);
        return v_p;
      }

      void add_edge_stereo (int edgeId_i, g2o::VertexCam* pose, g2o::VertexPointXYZ* point, Eigen::Vector3d measurement) {
        //see if anything needs be done here
        g2o::EdgeProjectP2SC* e = new g2o::EdgeProjectP2SC();
        e->setId(edgeId_i);
        e->setMeasurement(measurement);
        e->information() = Eigen::Matrix3d::Identity();
        e->vertices()[0] = point;
        e->vertices()[1] = pose;

        g2o::RobustKernelHuber* rk = new g2o::RobustKernelHuber;
        e->setRobustKernel(rk);
        rk->setDelta(robustCostFunctionDelta);

        optimizer_o.addEdge(e);
      }

      void optimize (int maxIter_i=10) {
        optimizer_o.initializeOptimization();
        optimizer_o.optimize(maxIter_i);
      }

      Eigen::Isometry3d get_pose (g2o::HyperGraph::Vertex &vertex) {
        g2o::VertexCam &vertex_view = dynamic_cast<g2o::VertexCam&>(vertex);
        Eigen::Vector3d trans = vertex_view.estimate().translation();
        const Eigen::Quaterniond &rot = vertex_view.estimate().rotation();

        Eigen::Isometry3d refinedPose_o = Eigen::Translation3d(trans) * rot;
        return refinedPose_o;
      }

      Eigen::Vector3d get_point (g2o::HyperGraph::Vertex &vertex) {
        g2o::VertexPointXYZ &point_vertex = dynamic_cast<g2o::VertexPointXYZ&>(vertex);
        return point_vertex.estimate();
      }

      void clear () {
        optimizer_o.clear();
      }


    private:


  };

  class Map {

    public:

      //vars
      std::vector<MapPoint> vMapPoints_o;
      std::vector<Eigen::Vector3d> vPoses_o;

      Map () {

      }

      void add_mappoint (MapPoint mapPoint_o) {
        vMapPoints_o.push_back(mapPoint_o);
      }

      void add_pose (Eigen::Isometry3d pose_o) {
        Eigen::Vector3d camOrigin(0, 0, 0);
        Eigen::Vector3d pose = pose_o.inverse() * camOrigin;
        vPoses_o.push_back(pose);
      }

    private:

  };

  class StereoFrame {

    public:

      //vars
      long idx_l;
      double timestamp_d;
      cv::Mat leftImage_o;
      cv::Mat rightImage_o;
      std::vector<cv::KeyPoint> vKeyPointsLeft_o;
      std::vector<cv::KeyPoint> vKeyPointsRight_o;
      cv::Mat descriptorsLeft_o;
      cv::Mat descriptorsRight_o;
      std::vector<cv::DMatch> vRowMatches_o;  //prob useless
      cv::Mat projLeftRect_o;
      cv::Mat projRightRect_o;
      double frustumNear_d;
      double frustumFar_d;
      Map* pMap_o;
      Eigen::Isometry3d pose_o;

      StereoFrame (long idx_l, cv::Mat leftImage_o, cv::Mat rightImage_o, double timestamp_d, cv::Mat projLeftRect_o, cv::Mat projRightRect_o, double frustumNear_d, double frustumFar_d) : idx_l(idx_l), leftImage_o(leftImage_o), rightImage_o(rightImage_o), timestamp_d(timestamp_d), projLeftRect_o(projLeftRect_o), projRightRect_o(projRightRect_o), frustumNear_d(frustumNear_d), frustumFar_d(frustumFar_d) {
        detect_features();  //GFTT
        compute_descriptors();  //BRISK
        row_match();  //feature matching (only matches in neighboring rows are taken)

        //this is temp stuff, remove it
        Eigen::Translation3d t(2, 0, 0);
        pose_o = t;
      }

      //copy constructor to use in stereokeyframe
      StereoFrame (const StereoFrame &frame_o) : idx_l(frame_o.idx_l), timestamp_d(frame_o.timestamp_d), leftImage_o(frame_o.leftImage_o), rightImage_o(frame_o.rightImage_o), vKeyPointsLeft_o(frame_o.vKeyPointsLeft_o), vKeyPointsRight_o(frame_o.vKeyPointsRight_o), descriptorsLeft_o(frame_o.descriptorsLeft_o), descriptorsRight_o(frame_o.descriptorsRight_o), projLeftRect_o(frame_o.projLeftRect_o), projRightRect_o(frame_o.projRightRect_o), frustumNear_d(frame_o.frustumNear_d), frustumFar_d(frame_o.frustumFar_d), pMap_o(frame_o.pMap_o), pose_o(frame_o.pose_o) {

      }

      //triangulate unmatched (to mappoints) keypoint pairs
      void triangulate_new_mappoints () {
        std::vector<int> vUnmatchedPairIdxs = get_unmatched_pairs();
        if (vUnmatchedPairIdxs.size() <= 0) {
          return;
        }
        std::vector<cv::Point2f> vPoints2dLeft_o;
        std::vector<cv::Point2f> vPoints2dRight_o;

        for (int i : vUnmatchedPairIdxs) {
          vPoints2dLeft_o.push_back(vKeyPointsLeft_o.at(i).pt);
          vPoints2dRight_o.push_back(vKeyPointsRight_o.at(i).pt);
        }

        cv::Mat points4d_o;
        cv::Mat points3d_o;
        std::vector<cv::Point3f> vPoints3d_o;
        cv::triangulatePoints(projLeftRect_o, projRightRect_o, vPoints2dLeft_o, vPoints2dRight_o, points4d_o);
        cv::convertPointsFromHomogeneous(points4d_o.t(), vPoints3d_o);
        //cull some bad points and add the rest
        for (int i = 0; i < vPoints3d_o.size(); ++i) {
          if (vPoints3d_o.at(i).z >= 0) {
            //since pose_o is the transformation to be applied on a world coordinate point to convert it to this frame's coordinate system
            Eigen::Isometry3d poseInverse_o = pose_o.inverse();
            Eigen::Vector3d position = poseInverse_o * Eigen::Vector3d(vPoints3d_o.at(i).x, vPoints3d_o.at(i).y, vPoints3d_o.at(i).z);
            cv::Mat descriptor = descriptorsLeft_o.row(i);
            MapPoint tempPoint_o(position, descriptor);
            pMap_o->add_mappoint(tempPoint_o);
            //see later : if keyframe obs needs to be added to mappoint or something
          }
        }
      }

      //get all mappoints in frustum which have matches in the keyframe
      //returns mappoint and index of the keypoints it is matched to
      //used in refine_pose(optimization)
      std::vector<std::pair<MapPoint, int> > get_matched_pairs () {
        std::vector<std::pair<MapPoint, int> > vRet_p;

        std::vector<MapPoint> vMapPointsInFrustum_o = get_map_points_in_frustum();
        cv::Mat mapDescriptors_o;
        for (MapPoint point : vMapPointsInFrustum_o) {
          mapDescriptors_o.push_back(point.descriptor_o);
        }

        std::vector<cv::DMatch> vTempMatches_o;
        cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create(cv::NORM_L2, true);
        matcher->match(mapDescriptors_o, descriptorsLeft_o, vTempMatches_o);

        for (cv::DMatch match : vTempMatches_o) {
          MapPoint mappoint = vMapPointsInFrustum_o.at(match.queryIdx); 
          int keypointIdx = match.trainIdx;
          std::pair<MapPoint, int> pair (mappoint, keypointIdx);
          vRet_p.push_back(pair);
        }

        return vRet_p;
      }

    protected:
      void detect_features () {
        cv::Ptr<cv::GFTTDetector> detector = cv::GFTTDetector::create();
        detector->detect(leftImage_o, vKeyPointsLeft_o);
        detector->detect(rightImage_o, vKeyPointsRight_o);
      }

      void compute_descriptors () {
        cv::Ptr<cv::BRISK> extractor = cv::BRISK::create();
        extractor->compute(leftImage_o, vKeyPointsLeft_o, descriptorsLeft_o);
        extractor->compute(rightImage_o, vKeyPointsRight_o, descriptorsRight_o);
      }

      //match keypoints in row and replace keypoint vector and descriptor matrix with matched points
      void row_match () {
        std::vector<cv::DMatch> vTempMatches_o;
        cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create(cv::NORM_L2, true);
        matcher->match(descriptorsLeft_o, descriptorsRight_o, vTempMatches_o);
        std::vector<cv::KeyPoint> vMatchedKeyPointsLeft_o;
        std::vector<cv::KeyPoint> vMatchedKeyPointsRight_o;
        for (cv::DMatch match : vTempMatches_o) {
          int idxLeft = match.queryIdx;
          int idxRight = match.trainIdx;
          float yLeft = vKeyPointsLeft_o.at(idxLeft).pt.y;
          float yRight = vKeyPointsRight_o.at(idxRight).pt.y;
          if (abs(yLeft - yRight) <= 10) {
            vRowMatches_o.push_back(match);
            vMatchedKeyPointsLeft_o.push_back(vKeyPointsLeft_o.at(idxLeft));
            vMatchedKeyPointsRight_o.push_back(vKeyPointsRight_o.at(idxRight));
          }
        }

        cv::Mat matchedDescriptorsLeft_o;
        cv::Mat matchedDescriptorsRight_o;
        cv::Ptr<cv::BRISK> extractor = cv::BRISK::create();
        extractor->compute(leftImage_o, vMatchedKeyPointsLeft_o, matchedDescriptorsLeft_o);
        extractor->compute(rightImage_o, vMatchedKeyPointsRight_o, matchedDescriptorsRight_o);

        vKeyPointsLeft_o = vMatchedKeyPointsLeft_o;
        vKeyPointsRight_o = vMatchedKeyPointsRight_o;
        descriptorsLeft_o = matchedDescriptorsLeft_o;
        descriptorsRight_o = matchedDescriptorsRight_o;
      }

      //get mappoints observable by both cameras
      std::vector<MapPoint> get_map_points_in_frustum () {
        std::vector<MapPoint> vRet;
        for (MapPoint point : pMap_o->vMapPoints_o) {
          if (point.position_o.z() <= frustumFar_d && point.position_o.z() >= frustumNear_d) {
            Eigen::MatrixXd eigenProjLeft_o(3, 4);
            cv::cv2eigen(projLeftRect_o, eigenProjLeft_o);
            Eigen::MatrixXd eigenProjRight_o(3, 4);
            cv::cv2eigen(projRightRect_o, eigenProjRight_o);

            //convert mappoint pos from global coord to local reference cam coord
            Eigen::Vector3d posInReferenceCamCoord = pose_o * point.position_o;
            Eigen::Vector4d positionTemp_o (posInReferenceCamCoord[0], posInReferenceCamCoord[1], posInReferenceCamCoord[2], 1);

            Eigen::Vector3d point2dLeft_o = eigenProjLeft_o * positionTemp_o;
            Eigen::Vector3d point2dRight_o = eigenProjRight_o * positionTemp_o;

            //points are in homogeneous it seems idk, so need convert
            point2dLeft_o = point2dLeft_o / point2dLeft_o[2];
            point2dRight_o = point2dRight_o / point2dRight_o[2];

            if (point2dLeft_o[0] >= 0 && point2dLeft_o[0] <= leftImage_o.size().width && point2dLeft_o[1] >= 0 && point2dLeft_o[1] <= leftImage_o.size().height) {
              if (point2dRight_o[0] >= 0 && point2dRight_o[0] <= rightImage_o.size().width && point2dRight_o[1] >= 0 && point2dRight_o[1] <= rightImage_o.size().height) {
                vRet.push_back(point);
              }
            }
          }
        }

        return vRet;
      }

      //get matched keypoints that don't match with any observable mappoint
      //used in triangulate_new_points
      std::vector<int> get_unmatched_pairs () {
        std::vector<int> vRetIdxs_i;
        std::vector<MapPoint> vMapPointsInFrustum_o = get_map_points_in_frustum();
        cv::Mat mapDescriptors_o;
        for (MapPoint point : vMapPointsInFrustum_o) {
          mapDescriptors_o.push_back(point.descriptor_o);
        }

        std::vector<cv::DMatch> vTempMatches_o;
        std::vector<bool> vIsMatchedToMapPoint_b(vKeyPointsLeft_o.size(), false);
        cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create(cv::NORM_L2, true);
        matcher->match(mapDescriptors_o, descriptorsLeft_o, vTempMatches_o);
        //flag the keypoints which match some mappoint
        for (cv::DMatch match : vTempMatches_o) {
          //check
          vIsMatchedToMapPoint_b.at(match.trainIdx) = true;
        }
        for (int i = 0; i < vKeyPointsLeft_o.size(); ++i) {
          if (!vIsMatchedToMapPoint_b.at(i)) {
            vRetIdxs_i.push_back(i);
          }
        }

        return vRetIdxs_i;
      }
  };

  class StereoKeyFrame;
  class CovisibilityGraph {

    public:

      //vars
      std::vector<StereoKeyFrame*> vpKeyFrames_o; 

      //dodo
      CovisibilityGraph () {

      }

      void add_keyframe (StereoKeyFrame* pKeyFrame_o) {
        vpKeyFrames_o.push_back(pKeyFrame_o);
      }


    private:

  };

  class StereoKeyFrame : public StereoFrame {

    public:

      //vars
      std::vector<StereoKeyFrame*> vpCovisibleKeyFrames;
      CovisibilityGraph* pCovisGraph_o;

      StereoKeyFrame (StereoFrame frame_o, CovisibilityGraph* pCovisGraph_o) : StereoFrame(frame_o), pCovisGraph_o(pCovisGraph_o) {
        //dodo
        get_covisible();
      }

      //add (pointer to this) stereokeyframe to covisibility graph
      void add_to_covisibility_graph () {
        pCovisGraph_o->add_keyframe(this);
      }

    private:

      //use covisibility graph to insert covisible stereokeyframes into above vector
      void get_covisible () {
        //dodo

      }

  };

  class Mapper; //forward declaration to avoid circular reference error
  class Tracker {

    public:

      //vars
      Map* pMap_o;
      Mapper* pMapper_o;
      cv::Mat projLeftRect_o;
      cv::Mat projRightRect_o;
      Eigen::Isometry3d currPose_o;
      Eigen::Isometry3d currVel_o;  //not actually used as transform, just to store r and t
      double currTimestamp_d;
      double decay_d = 0.90;  //damping factor for decaying velocity motion model
      Optimizer* optim_o;

      Tracker (cv::Mat projLeftRect_o, cv::Mat projRightRect_o) : projLeftRect_o(projLeftRect_o), projRightRect_o(projRightRect_o) {
        double focal1 = projLeftRect_o.at<double>(0, 0);
        double focal2 = projLeftRect_o.at<double>(1, 1);
        double point1 = projLeftRect_o.at<double>(0, 2);
        double point2 = projLeftRect_o.at<double>(1, 2);
        double baseline = -projLeftRect_o.at<double>(0, 3) / focal1;
        optim_o = new Optimizer(focal1, focal2, point1, point2, baseline);

        Eigen::Translation3d trans(0, 0, 0);
        currPose_o = trans;
        currVel_o = trans;
      }

      void initialize (StereoFrame* pFrame_o) {
        pFrame_o->pose_o = currPose_o;
        pFrame_o->triangulate_new_mappoints();
        update_motion_model(pFrame_o->timestamp_d, pFrame_o->pose_o);
        currTimestamp_d = pFrame_o->timestamp_d;

        pMap_o->add_pose(currPose_o);
      }

      void track (StereoFrame* pFrame_o) {
        Eigen::Isometry3d predictedPose_o = apply_motion_model(pFrame_o->timestamp_d);
        pFrame_o->pose_o = predictedPose_o; //because some pose estimate is needed for both triangulation and getting mappoints in frustum
        Eigen::Isometry3d refinedPose_o = refine_pose(pFrame_o, predictedPose_o);
        update_motion_model(pFrame_o->timestamp_d, refinedPose_o);
        pFrame_o->pose_o = refinedPose_o;
        currTimestamp_d = pFrame_o->timestamp_d;

        pMap_o->add_pose(currPose_o);
      }


    private:

      //check out 
      Eigen::Isometry3d apply_motion_model (double timestamp_d) {
        double dt = timestamp_d - currTimestamp_d;

        //change in rotation
        Eigen::AngleAxisd deltaRotation(currVel_o.rotation());
        double deltaAngle = deltaRotation.angle() * dt * decay_d;
        Eigen::Vector3d deltaAxis = deltaRotation.axis();
        deltaRotation = Eigen::AngleAxisd(deltaAngle, deltaAxis);

        //change in translation
        Eigen::Vector3d deltaTranslation (currVel_o.translation());
        deltaTranslation *= dt * decay_d;

        Eigen::Translation3d newTranslation(Eigen::Vector3d(currPose_o.translation()) + deltaTranslation);
        Eigen::Quaterniond newRotation = Eigen::Quaterniond(currPose_o.rotation()) * Eigen::Quaterniond(deltaRotation);

        return newTranslation * newRotation;
      }

      //check out 
      void update_motion_model (double timestamp_d, Eigen::Isometry3d newPose_o) {
        double dt = timestamp_d - currTimestamp_d;
        Eigen::Vector3d deltaTranslation = (Eigen::Vector3d(newPose_o.translation()) - Eigen::Vector3d(currPose_o.translation())) / dt;

        Eigen::Quaterniond deltaRotation = Eigen::Quaterniond(currPose_o.rotation().inverse()) * Eigen::Quaterniond(newPose_o.rotation());
        Eigen::AngleAxisd tempDeltaRotation (Eigen::AngleAxisd(deltaRotation).angle() / dt, Eigen::AngleAxisd(deltaRotation).axis()); //this is all just to divide angle by dt
        deltaRotation = Eigen::Quaterniond(tempDeltaRotation);

        currVel_o = Eigen::Translation3d(deltaTranslation) * deltaRotation;
        currPose_o = newPose_o;
      }

      Eigen::Isometry3d refine_pose (StereoFrame* pFrame_o, Eigen::Isometry3d predictedPose_o) {
        g2o::VertexPointXYZ* pAddedPoint;
        g2o::VertexCam* pAddedPose;

        optim_o->clear();
        pAddedPose = optim_o->add_pose(predictedPose_o, false);

        int edgeId_i = 0;

        std::vector<std::pair<MapPoint, int> > vMeasurementPairs = pFrame_o->get_matched_pairs();

        if (vMeasurementPairs.size() < 1) {
          return predictedPose_o;
        }

        for (std::pair<MapPoint, int> pair : vMeasurementPairs) {
          int kpIdx = pair.second;
          cv::Point2f leftKeyPointPos_f = pFrame_o->vKeyPointsLeft_o.at(kpIdx).pt;
          cv::Point2f rightKeyPointPos_f = pFrame_o->vKeyPointsRight_o.at(kpIdx).pt;

          Eigen::Vector3d measurement(leftKeyPointPos_f.x, leftKeyPointPos_f.y, rightKeyPointPos_f.x);

          pAddedPoint = optim_o->add_point(pair.first, true);
          optim_o->add_edge_stereo(edgeId_i, pAddedPose, pAddedPoint, measurement);  //add edge between pose and point
          ++edgeId_i;
        }

        optim_o->optimize();
        return optim_o->get_pose(*pAddedPose);
      }

  };

  class Mapper {

    public:

      //vars
      Map* pMap_o;
      Tracker* pTracker_o;
      CovisibilityGraph* pCovisGraph_o;

      Mapper () {

      }

      //dodo
      void insert_keyframe (StereoFrame* pFrame_o) {
        pFrame_o->triangulate_new_mappoints();
        StereoKeyFrame kf(*pFrame_o, pCovisGraph_o);
        kf.add_to_covisibility_graph();
      }

    private:

  };

  class Visualizer {

    public:

      //vars
      Map* pMap_o;
      bool mapSet_b;

      Visualizer () {
        mapSet_b = false;
      }

      void visualize () {
        pangolin::CreateWindowAndBind("vis", 640, 480);
        glEnable(GL_DEPTH_TEST);
        pangolin::OpenGlRenderState s_cam(
          pangolin::ProjectionMatrix(640, 480, 1006.9, 1006.9, 320, 240, 0.2, 1000),
          pangolin::ModelViewLookAt(-2, 2, -2, 0.0, 0.0, 0.0, pangolin::AxisY)
        );
        pangolin::Handler3D handler(s_cam);
        pangolin::View &d_cam = pangolin::CreateDisplay().SetBounds(0, 1, 0, 1, -640.0f/480.0f).SetHandler(&handler);
        while (!pangolin::ShouldQuit()) {
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
          d_cam.Activate(s_cam);
          std::vector<Eigen::Matrix<double, 3, 1> > vTempMapPoints;
          std::vector<Eigen::Matrix<double, 3, 1> > vTempPoses;
          
          if (!mapSet_b) {
            continue;
          }
          else {
            for (MapPoint point : pMap_o->vMapPoints_o) {
              Eigen::Matrix<double, 3, 1> pos(point.position_o / 400);
              vTempMapPoints.push_back(pos);
            }

            for (Eigen::Vector3d pose : pMap_o->vPoses_o) {
              Eigen::Matrix<double, 3, 1> pos(pose / 400);
              vTempPoses.push_back(pos);
            }

            glPointSize(5);
            glColor3f(0, 1, 0);
            pangolin::glDrawPoints(vTempMapPoints);

            glPointSize(8);
            glColor3f(1, 0, 0);
            pangolin::glDrawPoints(vTempPoses);
          }
          pangolin::FinishFrame();
        }
      }

      void set_map (Map* pMap_o) {
        this->pMap_o = pMap_o;
        mapSet_b = true;
        std::cout<<"set";
      }

    private:

  };

  class System {

    public:

      //vars
      Tracker* pTracker_o;
      Mapper* pMapper_o;
      Map* pMap_o;
      CovisibilityGraph* pCovisGraph_o;
      Visualizer* pVis_o;
      std::vector<std::string> vPathsLeft_s;
      std::vector<std::string> vPathsRight_s;
      cv::Mat projLeftRect_o;
      cv::Mat projRightRect_o;
      double frustumNear_d;
      double frustumFar_d;
      long idxGenerator_l;
      long kfInsertThresh; //no of frames after which a frame is passed to mapper

      System (std::vector<std::string> vPathsLeft_s, std::vector<std::string> vPathsRight_s, cv::Mat projLeftRect_o, cv::Mat projRightRect_o, double frustumNear_d, double frustumFar_d, Visualizer* pVis_o) : vPathsLeft_s(vPathsLeft_s), vPathsRight_s(vPathsRight_s), projLeftRect_o(projLeftRect_o), projRightRect_o(projRightRect_o), frustumNear_d(frustumNear_d), frustumFar_d(frustumFar_d), pVis_o(pVis_o) {
        pTracker_o = new Tracker(projLeftRect_o, projRightRect_o);
        pMapper_o = new Mapper;
        pMap_o = new Map;
        pCovisGraph_o = new CovisibilityGraph;
        pVis_o->set_map(pMap_o);

        pTracker_o->pMap_o = pMap_o;
        pTracker_o->pMapper_o = pMapper_o;
        pMapper_o->pMap_o = pMap_o;
        pMapper_o->pTracker_o = pTracker_o;
        pMapper_o->pCovisGraph_o = pCovisGraph_o;

        idxGenerator_l = 0;
        kfInsertThresh = 1;

        isInitialized_b = false;

      }

      ~System () {
        delete pTracker_o;
        delete pMapper_o;
        delete pMap_o;
      }

      void run () {
        int num_pairs = vPathsLeft_s.size();
        auto iterLeft = vPathsLeft_s.begin();
        auto iterRight = vPathsRight_s.begin();

        time_point start = steady_clock::now();

        while (num_pairs--) {
          //create frame
          cv::Mat imageLeft_o = cv::imread(*iterLeft);
          cv::Mat imageRight_o = cv::imread(*iterRight);
          time_point curr = steady_clock::now();
          duration timestamp = std::chrono::duration_cast<duration> (curr - start);
          sslam::StereoFrame frame_o(idxGenerator_l++, imageLeft_o, imageRight_o, timestamp.count(), projLeftRect_o, projRightRect_o, frustumNear_d, frustumFar_d);
          frame_o.pMap_o = pMap_o;

          //init if not
          if (!isInitialized_b) {
            pTracker_o->initialize(&frame_o);
            isInitialized_b = true;
          }
          else {
            pTracker_o->track(&frame_o);
          }

          //insert frame into mapper
          if (idxGenerator_l % kfInsertThresh == 0) {
            pMapper_o->insert_keyframe(&frame_o);
          }

          ++iterLeft;
          ++iterRight;

        }
      }
  
    private:
      bool isInitialized_b;


  };

}

//----------------------------------------
//main
//----------------------------------------

int main() {

  //test eigen
  Eigen::Quaternion<float> r;
  Eigen::Translation3f t(2, 0, 3);
  Eigen::Isometry3f pose;
  pose = t;
  pose.rotate(r);
  Eigen::Vector3f trans(2, 2, 2);
  pose.pretranslate(trans);

  //image filepaths
  std::vector<std::string> vPathsLeft_s = sslam::Utilities::get_paths_from_dir ("../data/kitti_00/00/image_2");
  std::vector<std::string> vPathsRight_s = sslam::Utilities::get_paths_from_dir ("../data/kitti_00/00/image_3");

  //sslam::Utilities::preview_stereo_data(vPathsLeft_s, vPathsRight_s, 10);

  //camera info
  double PLeftRect [] = {1.006938e+3, 0.000000e+0, 4.530236e+2, 0.000000e+0, 0.000000e+0, 1.006938e+3, 1.987890e+2, 0.000000e+0, 0.000000e+0, 0.000000e+0, 1.000000e+0, 0.000000e+0};
  double PRightRect [] = {1.006938e+3, 0.000000e+0, 4.530236e+2, -1.097446e+3, 0.000000e+0, 1.006938e+3, 1.987890e+2, 0.000000e+0, 0.000000e+0, 0.000000e+0, 1.000000e+0, 0.000000e+0};
  cv::Mat projLeftRect_o = cv::Mat(3, 4, CV_64F, PLeftRect);
  cv::Mat projRightRect_o = cv::Mat(3, 4, CV_64F, PRightRect);
  double frustumNear_d = 0.1;
  double frustumFar_d = 1000;

  sslam::Visualizer* pVis_o = new sslam::Visualizer;

  sslam::System* pSystem_o = new sslam::System(vPathsLeft_s, vPathsRight_s, projLeftRect_o, projRightRect_o, frustumNear_d, frustumFar_d, pVis_o);
  std::thread tSystem_o(&sslam::System::run, pSystem_o);

  pVis_o->visualize();

  delete pSystem_o;
  delete pVis_o;

  return 0;
}
