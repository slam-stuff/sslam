//----------------------------------------
//imports
//----------------------------------------
#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <chrono>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <unistd.h>

#include <opencv2/opencv.hpp>
#include <boost/filesystem.hpp>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/types/slam3d/types_slam3d.h>
#include <g2o/solvers/csparse/linear_solver_csparse.h>
#include <g2o/solvers/cholmod/linear_solver_cholmod.h>
#include <g2o/core/sparse_optimizer_terminate_action.h>
#include <g2o/types/icp/types_icp.h>
#include <g2o/types/sba/types_six_dof_expmap.h>
#include <g2o/types/sba/types_sba.h>
#include <g2o/core/robust_kernel_impl.h>
#include <pangolin/pangolin.h>
#include <opencv2/core/eigen.hpp>

typedef std::chrono::steady_clock steady_clock;
typedef std::chrono::steady_clock::time_point time_point;
typedef std::chrono::duration<double> duration;
